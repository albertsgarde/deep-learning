import torch
from torch.autograd import Variable
from torch.utils.data import DataLoader
import numpy as np
import itertools

use_cuda = None
device = None

def setup_device(use_cuda_if_possible: bool):
    global use_cuda
    global device
    use_cuda = torch.cuda.is_available() and use_cuda_if_possible
    device = torch.device("cuda" if use_cuda else "cpu")
    print("Running GPU.") if use_cuda else print("No GPU available.")
    return device, use_cuda

def to_numpy(x):
    """ Get numpy array for both cuda and not. """
    global use_cuda
    if isinstance(x, np.ndarray):
        return x
    elif isinstance(x, torch.Tensor):
        if use_cuda:
            return x.cpu().data.numpy()
        return x.data.numpy()
    else:
        raise Exception(f"Unsupported type for to_numpy: {type(x)}")

def to_torch(x):
    global use_cuda
    if isinstance(x, torch.Tensor):
        if use_cuda:
            x = x.cuda()
        return x
    elif isinstance(x, np.ndarray):
        variable = Variable(torch.from_numpy(x))
        if use_cuda:
            variable = variable.cuda()
        return variable
    else:
        raise Exception(f"Unsupported type for to_torch: {type(x)}")

def mean_minibatch_err(output, target, label, error_function):
    #assert output.shape == target.shape, f"Output and target must be same shape. Output shape: {output.shape}  target shape: {target.shape}"
    total = 0
    for i, output_row in enumerate(output):
        total += error_function(output_row, target[i], label[i])
    return total/output.shape[0]

def test_net(net: torch.nn.Module, validation_loader: DataLoader, criterion, num_validation_batches: int, eval_funcs):
    r"""
        Args:
            net: the model to test.
            validation_loader: a data loader that outputs validation data.
            criterion: the loss function.
            num_validation_batches: how many batches to validate the model on.
            error_functions: a list of functions taking the model output and the target and returning a floating point error measure.
    """
    was_training = net.training
    net.eval()
    total_loss = 0
    total_errors = [0] * len(eval_funcs)
    for input, labels in itertools.islice(validation_loader, num_validation_batches):
        input = input.to(device)
        labels = labels.to(device)
        output = net(input)
        
        total_loss += criterion(output, labels).item()

        output = to_numpy(output)
        labels = to_numpy(labels)
        for i, eval_func in enumerate(eval_funcs):
            func = eval_func["func"]

            total_errors[i] += mean_minibatch_err(output, labels, labels, func)

    net.train(mode=was_training)
    return total_loss/num_validation_batches, list(map(lambda x: x / num_validation_batches, total_errors))

class ErrorTracker:
    def __init__(self, criterion, eval_funcs, num_validation_batches: int):
        self.criterion = criterion
        self.eval_funcs = eval_funcs
        self.num_validation_batches = num_validation_batches

        self.train_log_losses = []
        self.train_errors = [[] for _ in range(len(eval_funcs))]
        self.train_iter = []

        self.val_log_losses = []
        self.val_errors = [[] for _ in range(len(eval_funcs))]
        self.val_iter = []
    
    def training_update(self, index: int, output, target, label, loss):
        log_loss = np.log10(loss.item())
        self.train_log_losses.append(log_loss)


        output = to_numpy(output)
        target = to_numpy(target)
        for i, eval_func in enumerate(self.eval_funcs):
            func = eval_func["func"]

            self.train_errors[i].append(mean_minibatch_err(output, target, label, func))
        self.train_iter.append(index)

    def validation_update(self, index: int, net: torch.nn.Module, validation_loader: DataLoader):
        val_loss, val_errors = test_net(net, validation_loader, self.criterion, self.num_validation_batches, self.eval_funcs)
        self.val_log_losses.append(np.log10(val_loss))
        for i in range(len(self.val_errors)):
            self.val_errors[i].append(val_errors[i])
        self.val_iter.append(index)
    
    def train_history(self):
        return self.train_iter, self.train_log_losses, self.train_errors
    
    def validation_history(self):
        return self.val_iter, self.val_log_losses, self.val_errors

    def train_history_table(self):
        assert len(self.train_iter) == len(self.train_log_losses)
        train_errors = list(map(list, zip(*self.train_errors)))
        assert len(self.train_iter) == len(train_errors)
        return [[self.train_iter[i], self.train_log_losses[i], *train_errors[i]] for i in range(len(self.train_iter))]
    
    def validation_history_table(self):
        assert len(self.val_iter) == len(self.val_log_losses)
        val_errors = list(map(list, zip(*self.val_errors)))
        assert len(self.val_iter) == len(val_errors)
        return [[self.val_iter[i], self.val_log_losses[i], *val_errors[i]] for i in range(len(self.val_iter))]